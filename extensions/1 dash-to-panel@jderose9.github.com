# Name: Dash to Panel
# Description: An icon taskbar for the Gnome Shell. This extension moves the dash into the gnome main panel so that the application launchers and system tray are combined into a single panel, similar to that found in KDE Plasma and Windows 7+. A separate dock is no longer needed for easy access to running and favorited applications.
# URL: https://github.com/home-sweet-gnome/dash-to-panel
# Version: 42
# UUID: dash-to-panel@jderose9.github.com
# Schema Settings: /org/gnome/shell/extensions/dash-to-panel/
[/]
animate-appicon-hover-animation-extent={'RIPPLE': 4, 'PLANK': 4, 'SIMPLE': 1}
available-monitors=[0]
hotkeys-overlay-combo='TEMPORARILY'
panel-anchors='{"0":"MIDDLE"}'
panel-element-positions='{"0":[{"element":"showAppsButton","visible":false,"position":"stackedTL"},{"element":"activitiesButton","visible":false,"position":"stackedTL"},{"element":"leftBox","visible":true,"position":"stackedTL"},{"element":"taskbar","visible":false,"position":"stackedTL"},{"element":"centerBox","visible":true,"position":"centered"},{"element":"rightBox","visible":true,"position":"centered"},{"element":"dateMenu","visible":true,"position":"stackedBR"},{"element":"systemMenu","visible":true,"position":"stackedBR"},{"element":"desktopButton","visible":false,"position":"stackedBR"}]}'
panel-lengths='{"0":100}'
panel-positions='{"0":"TOP"}'
panel-sizes='{"0":32}'
