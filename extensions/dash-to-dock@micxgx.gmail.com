# Name: Dash to Dock
# Description: A dock for the Gnome Shell. This extension moves the dash out of the overview transforming it in a dock for an easier launching of applications and a faster switching between windows and desktops. Side and bottom placement options are available.
# URL: https://micheleg.github.io/dash-to-dock/
# Original author: micxgx@gmail.com
# Version: 69
# UUID: dash-to-dock@micxgx.gmail.com
# Schema Settings: /org/gnome/shell/extensions/dash-to-dock/
[/]
apply-custom-theme=false
background-color='#000000'
background-opacity=0.5
custom-background-color=true
custom-theme-shrink=true
dock-position='BOTTOM'
extend-height=false
icon-size-fixed=false
isolate-workspaces=false
multi-monitor=true
show-apps-at-top=true
show-favorites=true
show-running=true
show-show-apps-button=true
show-trash=false
transparency-mode='DYNAMIC'
