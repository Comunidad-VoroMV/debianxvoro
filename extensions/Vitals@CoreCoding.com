# Name: Vitals
# Description: A glimpse into your computer's temperature, voltage, fan speed, memory usage, processor load, system resources, network speed and storage stats. This is a one stop shop to monitor all of your vital sensors. Uses asynchronous polling to provide a smooth user experience. Feature requests or bugs? Please use GitHub.
# URL: https://github.com/corecoding/Vitals
# Version: 54
# UUID: Vitals@CoreCoding.com
# Schema Settings: /org/gnome/shell/extensions/vitals/
[/]
position-in-panel=0
