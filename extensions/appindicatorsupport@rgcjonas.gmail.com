# Name: AppIndicator and KStatusNotifierItem Support
# Description: Adds AppIndicator, KStatusNotifierItem and legacy Tray icons support to the Shell
# URL: https://github.com/ubuntu/gnome-shell-extension-appindicator
# Version: 42
# UUID: appindicatorsupport@rgcjonas.gmail.com
# Schema Settings: /org/gnome/shell/extensions/appindicator/
