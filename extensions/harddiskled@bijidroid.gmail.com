# Name: Harddisk LED
# Description: Show harddisk activity (IO speed read/write and LED). Click to change led size
# URL: https://github.com/biji/harddiskled
# Version: 30
# UUID: harddiskled@bijidroid.gmail.com
# Schema Settings: /org/gnome/shell/extensions/harddiskled/
[/]
mode=2
