# DebianxVoro
Simple 'script' para dejar el escritorio de Gnome con la personalización de 'VoroMV'

> Basado en el video [Mi donación a Debian. Las extensiones que tengo instaladas en Debian 11. Instalación y explicación.
](https://www.youtube.com/watch?v=9WB1TTC_xcc)

## Instalacion
1. Descargué el comprimido del proyecto o clone el repositorio

> Para descargar el comprimido, vaya a los **releases** y seleccione la versión correspondiente a su versión de GNOME: [https://gitlab.com/Comunidad-VoroMV/debianxvoro/-/releases](https://gitlab.com/Comunidad-VoroMV/debianxvoro/-/releases)

2. De permisos de ejecución a 'debianxvoro.sh' mediante el emulador de terminar: ```chmod +x "debianxvoro.sh"```, o con el propio gestor de archivo.

3. Ejecuté 'debianxvoro.sh' interponiendo *bash* ```bash debianxvoro.sh``` o directamente como ```./debianxvoro.sh```. Se le pedira permiso para instalar las extensiónes

4. Cerrar sesión (recomendado para recargar las extensiones)

### Cambios basicos
- Establece el tema 'Adwaita-dark'
- Habilita los botones 'Maximizar' y 'Minimizar' en la barra superior
- Establece el fondo de escritorio 'Homeworld' de Debian 11 en escala de grises

### Instalacion y configuración de Extensiones
- "AppIndicator and KStatusNotifierItem Support"
- "Dash to Dock"
- "Dash to Panel"
- "Harddisk LED"
- "Sound Input & Output Device Chooser"
- "Time ++"
- "Vitals"

## Resultado

![](https://gitlab.com/Comunidad-VoroMV/debianxvoro/-/raw/main/debianxvoro-previa.png)
