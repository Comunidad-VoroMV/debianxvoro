#!/bin/bash
echo "GnomeXVoro"
read -n 1 -p "Vamos a configurar ... [Enter]"

# Establecer tema 'Adwaita-dark'
gsettings set org.gnome.desktop.interface gtk-theme "Adwaita-dark"

# Habilitar botones 'Maximizar' y 'Minimizar' en la barra superior
gsettings set org.gnome.desktop.wm.preferences button-layout 'appmenu:minimize,maximize,close'

# Guardar y establecer fondo de pantalla
pictures_folder=$(xdg-user-dir PICTURES)
mkdir -p "$pictures_folder"
cp backgrounds/homeworld-gray.png "$pictures_folder"
gsettings set org.gnome.desktop.background picture-uri "file:////$pictures_folder/homeworld-gray.png"

echo "Instalación y configuración de extension(es) GNOME"
for file in ./extensions/*
do
	uuid=$(grep '# UUID:' "$file" | awk -F': ' '{print $2}')
	schema_settings=$(grep '# Schema Settings:' "$file" | awk -F': ' '{print $2}')
	status=$(gdbus call --session --dest org.gnome.Shell.Extensions --object-path /org/gnome/Shell/Extensions --method org.gnome.Shell.Extensions.InstallRemoteExtension "$uuid" 2>/dev/null)
	if [ "$status" == "('cancelled',)" ]; then
	 	echo "No se instalo $uuid"
	else
            schema=$(awk -F@ '{ print $1 }' <<< $uuid)
            dconf load "$schema_settings" < "$file"
 	     echo "$uuid instalado y configurado"
            sleep 1
	fi
done
